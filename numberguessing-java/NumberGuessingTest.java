import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NumberGuessingTest {

	protected NumberGuessing solution;

	@Before
	public void setUp() {
		solution = new NumberGuessing();
	}

	@Test
	public void testCase0() {
		int range = 1000;
		int[] guesses = new int[] { 500 };
		int numLeft = 1;

		int expected = 501;
		int actual = solution.bestGuess(range, guesses, numLeft);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase1() {
		int range = 1000000;
		int[] guesses = new int[] {};
		int numLeft = 1;

		int expected = 500000;
		int actual = solution.bestGuess(range, guesses, numLeft);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase2() {
		int range = 1000;
		int[] guesses = new int[] {};
		int numLeft = 2;

		int expected = 750;
		int actual = solution.bestGuess(range, guesses, numLeft);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase3() {
		int range = 100;
		int[] guesses = new int[] { 27, 80 };
		int numLeft = 1;

		int expected = 26;
		int actual = solution.bestGuess(range, guesses, numLeft);

		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testCase6() {
		int range = 20;
		int[] guesses = new int[] { 8, 13 };
		int numLeft = 2;

		int expected = 18;
		int actual = solution.bestGuess(range, guesses, numLeft);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase8() {
		int range = 20;
		int[] guesses = new int[] { 8, 13, 18, 3 };
		int numLeft = 0;

		int expected = 2;
		int actual = solution.bestGuess(range, guesses, numLeft);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase14() {
		int range = 10;
		int[] guesses = new int[] { 9, 6, 1, 2 };
		int numLeft = 2;

		int expected = 3;
		int actual = solution.bestGuess(range, guesses, numLeft);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase16() {
		int range = 10;
		int[] guesses = new int[] { 9, 6, 1, 2, 3, 4 };
		int numLeft = 0;

		int expected = 5;
		int actual = solution.bestGuess(range, guesses, numLeft);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase28() {
		int range = 720732;
		int[] guesses = new int[] { 270364, 706055, 718771, 231002, 572272,
				687510, 406934, 477034 };
		int numLeft = 1;

		int expected = 77001;
		int actual = solution.bestGuess(range, guesses, numLeft);

		Assert.assertEquals(expected, actual);
	}
}
