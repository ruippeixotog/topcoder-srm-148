
public class CircleGame {
	public int cardsLeft(String deck) {
		deck = deck.replace("K", "");

		boolean changed = true;
		while (changed) {
			changed = false;
			for (int i = 1; i < deck.length(); i++) {
				if (cardValue(deck.charAt(i - 1)) + cardValue(deck.charAt(i)) == 13) {
					deck = removePair(deck, i - 1);
					changed = true;
				}
			}
			if (deck.length() > 1 && cardValue(deck.charAt(0))
					+ cardValue(deck.charAt(deck.length() - 1)) == 13) {
				deck = deck.substring(1, deck.length() - 1);
				changed = true;
			}
		}
		return deck.length();
	}

	String removePair(String str, int i) {
		return str.substring(0, i) + str.substring(i + 2);
	}

	int cardValue(char c) {
		switch (c) {
		case 'A':
			return 1;
		case 'T':
			return 10;
		case 'J':
			return 11;
		case 'Q':
			return 12;
		default:
			return c - '2' + 2;
		}
	}
}
