public class CeyKaps {
	public String decipher(String typed, String[] switches) {
		int[] pos = new int[26];
		for(int i = 0; i < pos.length; i++) {
			pos[i] = i;
		}
		for(String s: switches) {
			String[] sw = s.split(":");
			char c1 = sw[0].charAt(0), c2 = sw[1].charAt(0);
			int i1 = indexOf(pos, c1 - 'A'), i2 = indexOf(pos, c2 - 'A');
			int temp = pos[i1];
			pos[i1] = pos[i2];
			pos[i2] = temp;
		}
		char[] solution = new char[typed.length()];
		for(int i = 0; i < typed.length(); i++) {
			solution[i] = (char) (pos[typed.charAt(i) - 'A'] + 'A');
		}
		return new String(solution);
	}
	
	int indexOf(int[] arr, int val) {
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == val)
				return i;
		}
		return -1;
	}
}