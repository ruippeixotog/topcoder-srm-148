public class DivisorDigits {
	public int howMany(int number) {
		char[] digits = ("" + number).toCharArray();
		int counter = 0;
		for(char c: digits) {
			int digit = Integer.parseInt(c + "");
			if(digit != 0 && number % digit == 0) {
				counter++; 
			}
		}
		return counter;
	}
}
