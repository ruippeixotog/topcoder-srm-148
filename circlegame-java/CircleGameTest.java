import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CircleGameTest {

    protected CircleGame solution;

    @Before
    public void setUp() {
        solution = new CircleGame();
    }

    @Test
    public void testCase0() {
        String deck = "KKKKKKKKKK";

        int expected = 0;
        int actual = solution.cardsLeft(deck);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase1() {
        String deck = "KKKKKAQT23";

        int expected = 1;
        int actual = solution.cardsLeft(deck);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase2() {
        String deck = "KKKKATQ23J";

        int expected = 6;
        int actual = solution.cardsLeft(deck);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase3() {
        String deck = "AT68482AK6875QJ5K9573Q";

        int expected = 4;
        int actual = solution.cardsLeft(deck);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase4() {
        String deck = "AQK262362TKKAQ6262437892KTTJA332";

        int expected = 24;
        int actual = solution.cardsLeft(deck);

        Assert.assertEquals(expected, actual);
    }

}
